#HandleJson module - Elidjay Ross & William Trudel
import json

class HandleJson:
    
    #CRAZY JSON FILTER I THINK I OVER COMPLICATED IT BUT IT WORKS
    def __init__(self,path):
        self.__path = path
    
    #GET JSON DATA
    def getJson(self):
        with open(self.__path) as jsonFile:
            data = json.load(jsonFile)
        return data    
    
    #JSON DATA TO LIST
    def jsonToList(self):
        keyList = []
        valuesList = []
        for i in self.getJson():
            keyList.append(str(i.keys()))
            valuesList.append(str(i.values()))
            
        keyList = self.cleanKeyList(keyList)
        valuesList = self.cleanValuesList(valuesList)
        
        outputList = self.getFinalList(keyList,valuesList)
        return outputList
    
    #CLEANS THE LIST OF KEYS
    def cleanKeyList(self,keyList):
        outputList = []
        for j in keyList:
            
            j = j.replace("dict_keys", "")
            j = j.translate({ord(i): "" for i in "["})
            j = j.translate({ord(i): "" for i in "]"})
            j = j.translate({ord(i): "" for i in "("})
            j = j.translate({ord(i): "" for i in ")"})
            j = j.translate({ord(i): "" for i in "'"})
            outputList.append(j)
        return outputList
    
    #CLEANS ALL THE VALUES
    def cleanValuesList(self,valuesList):
        outputList = []
        
        for j in valuesList:
            tempList = []
            tempList2 = []

            j = str(j)
            j = j.replace("dict_values", "")
            j = j.replace("dict_keys", "")
            j = j.translate({ord(i): "" for i in "{"})
            j = j.translate({ord(i): "" for i in "}"})
            j = j.translate({ord(i): "" for i in "["})
            j = j.translate({ord(i): "" for i in "]"})
            j = j.translate({ord(i): "" for i in "("})
            j = j.translate({ord(i): "" for i in ")"})
            j = j.translate({ord(i): "" for i in "'"})
            j = j.translate({ord(i): "" for i in '"'})
            tempList.append(str(j.split(": ")).split(", "))
            for i in tempList:
                tempList2 = []
                for h in i:
                    h = h.translate({ord(i): "" for i in "'"})
                    h = h.translate({ord(i): "" for i in '"'})
                    h = h.translate({ord(i): "" for i in "["})
                    h = h.translate({ord(i): "" for i in "]"})
                    tempList2.append(h)
                outputList.append(tempList2)
                
        return outputList
    
    #TAKES ALL THE CLEAN DATA AND MAKES DATA READY FOR SQL 
    def getFinalList(self,keyList,valuesList):
        outputList = []
        country = ""
        closeCountry = ""
        borderLenght = ""
        
        count = 0
        for i in valuesList:
            count2 = 0
            while(count2 <= (len(i)-1)):
                if len(i) > 1:
                    tempList = []
                    country = keyList[count]
                    closeCountry = i[count2]
                    count2 = count2+1
                    borderLenght = i[count2]
                    count2 = count2+1
                    tempList.append(country)
                    tempList.append(closeCountry)
                    tempList.append(float(borderLenght))
                    outputList.append(tuple(tempList))
                
                else:
                    count2 = count2+2
            count = count+1
        return outputList

                    
                
                    
        
        
        
    
    
    