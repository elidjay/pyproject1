# pyproject1

Scrape covid data from worldometer and transfer it to a sql database. Then from that data create graphs.

## Getting started
Please kill db connection if program has error or you force quit.
If nothing happens for more than 30 seconds please kill db connection and retry.
For graphs, I don't know how to figure out the names but here's the order:
1. Country
2. Country, Country With Longest Border
3. Country, Border 1 (Biggest), Border 2 (2nd Biggest), Border 3 (3rd Biggest.

Thank you!

## Name
Covid Web Scraper 


