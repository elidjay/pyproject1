#Elidjay Ross / William Trudel - Database API Module
import mysql.connector

class DB_CLASS:    
    #INIT METHOD
    def __init__(self,host,user,passwd,db_name):
        self.__host = host
        self.__user = user
        self.__passwd = passwd
        self.__db_name = db_name
        self.__connect()
    
    #CONNECTS TO THE DATABASE
    def __connect(self):
        try:
            self.__db_obj  = mysql.connector.connect(
                host=self.__host,
                user=self.__user,
                password=self.__passwd
                #autocommit=True
                )
        except mysql.connector.Error as err:
            print(err)
    
    #RETURN THE DB OBJ FOR DATA EXPLORATION
    def getDbObj(self):
        return self.__db_obj