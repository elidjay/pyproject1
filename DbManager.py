#Elidjay Ross / William Trudel - Database Management Module
from DbAPI import DB_CLASS
import os
class DbManager:    
    #INIT METHOD
    def __init__(self):
     self.__connect()       
        
    #CONNECTS TO DATABSE AND SELECTS IT
    def __connect(self):
        self.__dbClass = DB_CLASS(os.getenv("HOST","localhost"),os.getenv("USER","root"),os.getenv("PASSWD","Python_420"),os.getenv("DB_NAME","covid_corona_db_ELRO_WITR"))
        self.__dbObj = self.__dbClass.getDbObj()
        self.__cursor = self.__dbObj.cursor()
        self.__create_database(os.getenv("DB_NAME","covid_corona_db_ELRO_WITR"))
        self.__select_database(os.getenv("DB_NAME","covid_corona_db_ELRO_WITR"))
    
    #CREATES THE DATABASE
    def __create_database(self,db_name):
        self.__cursor.execute("CREATE DATABASE if not exists " + db_name + ";")
    
    #SELECTS THE DATABASE
    def __select_database(self,db_name):
        self.__cursor.execute("USE "+ db_name + ";")
        
    #CLOSES THE DATABASE AND CURSOR
    def close_database(self):
        self.__cursor.close()
        self.__dbObj.close()
    
    #CREATE A TABLE
    def create_table(self,table_name,table_schema):
        self.__cursor.execute("DROP TABLE IF EXISTS " + table_name)
        
        self.__cursor.execute("CREATE TABLE " + table_name + table_schema)
    
    #TAKES DATA AND POPULATES THE TABLE
    def populate_table(self,table_name, records_list):
        
        #CHOOSES BETWEEN IF ITS POPULATE SCRAPED DATA OR JSON
        if("day" in table_name):
            query = "INSERT INTO " + table_name + """(currentDate,rankNum,country,totalCases,newCases,totalDeaths,newDeaths,totalRecovered,newRecovered,
            activeCases,seriousCases,totalCasesPer1mPop,deathsPer1mPop,totalTests,testsPer1mPop,population,continent) 
            VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"""
        
        elif("country" in table_name):
            query = "INSERT INTO " + table_name + """(country,closeCountry,borderLenght) VALUES (%s,%s,%s);"""
        
        #POPULATE
        self.__cursor.executemany(query,records_list)
        self.__dbObj.commit()
    
    #RETURNS DB OBJ
    def getDbObj(self):
        return self.__dbObj