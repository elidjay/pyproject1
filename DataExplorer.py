#DataExplorer - Elidjay Ross William Trudel
import pandas as pd
from DataVisualizer import DataVisualizer

class DataExplorer:
    
    #INIT
    def __init__(self,dbObj):
        self.__dbObj = dbObj
        self.__visualizer = DataVisualizer()
    
    
    def evolutionOfCountry(self,country):
        #UGLY BUT ITS EITHER HERE OR OVER THERE AS ALL QUERIES ARE DIFFERENT :()
        query1 = "select currentDate,country,newCases,newDeaths,newRecovered from day1"
        query2 = "select currentDate,country,newCases,newDeaths,newRecovered from day2"
        query3 = "select currentDate,country,newCases,newDeaths,newRecovered from day3"
        query4 = "select currentDate,country,newCases,newDeaths,newRecovered from day4"
        query5 = "select currentDate,country,newCases,newDeaths,newRecovered from day5"
        query6 = "select currentDate,country,newCases,newDeaths,newRecovered from day6"
        
        #GET ALL DATA DF
        allData = self.getAllDfs(q1=query1,q2=query2,q3=query3,q4=query4,q5=query5,q6=query6)
        #SEARCHES FOR COUNTRY
        search = (allData["country"]==country)
        #CREATES DF WITH SEARCH
        out = allData[search]
        #FILTER DF
        result = out[["newCases","newDeaths","newRecovered"]]
        
        print(result)
        print(result.describe())

        #MAKES GRAPH
        try:
            self.__visualizer.visualize(result)
        except:
            print("Could not create graph")
        
    def evolutionOfCountryWithBorder(self,country):
        query1 = "select d.currentDate,d.country,d.newCases,cbt.closeCountry,cbt.borderLenght from day1 d inner join country_borders_table cbt on d.country = cbt.country"
        query2 = "select d.currentDate,d.country,d.newCases,cbt.closeCountry,cbt.borderLenght from day2 d inner join country_borders_table cbt on d.country = cbt.country"
        query3 = "select d.currentDate,d.country,d.newCases,cbt.closeCountry,cbt.borderLenght from day3 d inner join country_borders_table cbt on d.country = cbt.country"
        query4 = "select d.currentDate,d.country,d.newCases,cbt.closeCountry,cbt.borderLenght from day4 d inner join country_borders_table cbt on d.country = cbt.country"
        query5 = "select d.currentDate,d.country,d.newCases,cbt.closeCountry,cbt.borderLenght from day5 d inner join country_borders_table cbt on d.country = cbt.country"
        query6 = "select d.currentDate,d.country,d.newCases,cbt.closeCountry,cbt.borderLenght from day6 d inner join country_borders_table cbt on d.country = cbt.country"
        
        #GET ALL DATA
        allData = self.getAllDfs(q1=query1,q2=query2,q3=query3,q4=query4,q5=query5,q6=query6)
        
        #GET DF FOR COUNTRY
        searchForCountry = (allData["country"]==country)
        dfOfCountry = allData[searchForCountry]
        
        #GET LONGEST BORDER OF COUNTRY
        longestBorder = self.getLongestBorder(dfOfCountry,country)
        
        #SEARCH IN ALL DATA FOR BORDER
        searchForLongestBorder = (dfOfCountry["borderLenght"]==longestBorder) 
        dfOfLongestBorder=dfOfCountry[searchForLongestBorder]
        
        #GETS DATA OF COUNTRY WITH LONGEST BORDER
        countryWithLongestBorder = dfOfLongestBorder["closeCountry"][0]
        
        #SEARCHES FOR LONGEST BORDER DATA
        searchForLongestBorderData = (allData["country"]==countryWithLongestBorder)
        dfOfLongestBorderCountry = allData[searchForLongestBorderData]
        
        #SORTS DATA
        resultDf = dfOfCountry[["newCases","country"]]
        resultDf = resultDf[~resultDf.index.duplicated(keep='first')]

        #SORTS DATA
        resultDf2= dfOfLongestBorderCountry[["newCases","country"]]
        resultDf2 = resultDf2[~resultDf2.index.duplicated(keep='first')]
        
        #PUT ALL DF TOGETHER
        finalDf = pd.concat([resultDf,resultDf2],axis=1)
        
        print(finalDf)
        print(finalDf.describe())
        
        #GRAPHS IT
        try:
            self.__visualizer.visualize(finalDf)
        except:
            print("Could not create graph")
        
    def evolutionOfDeaths(self,country):
        query1 = "select d.currentDate,d.country,cbt.closeCountry,cbt.borderLenght,d.deathsPer1mPop from day1 d inner join country_borders_table cbt on d.country = cbt.country"
        query2 = "select d.currentDate,d.country,cbt.closeCountry,cbt.borderLenght,d.deathsPer1mPop from day2 d inner join country_borders_table cbt on d.country = cbt.country"
        query3 = "select d.currentDate,d.country,cbt.closeCountry,cbt.borderLenght,d.deathsPer1mPop from day3 d inner join country_borders_table cbt on d.country = cbt.country"
       
        allData = self.getAllDfs(q1=query1,q2=query2,q3=query3)
        
        searchForCountry = (allData["country"]==country)
        dfOfCountry = allData[searchForCountry]
        
        #SORTS TEMP DF
        tempDf = dfOfCountry.sort_values(by=["borderLenght"],ascending=False)
        tempDf.reset_index()
        closeCountryList = []
        #GETS 3 CLOSEST COUNTRIES WITH LONGEST BORDER
        count = 1
        if len(tempDf) >= 3:
            while(count <= len(tempDf)/3 or count < 9):
                closeCountryList.append(tempDf["closeCountry"][count])
                count = count + 3
            searchForCloseCountry1 = (allData["country"]==closeCountryList[0])
            searchForCloseCountry2 = (allData["country"]==closeCountryList[1])
            searchForCloseCountry3 = (allData["country"]==closeCountryList[2])            
        
        closeCountryDf1= allData[searchForCloseCountry1]
        closeCountryDf2= allData[searchForCloseCountry2]           
        closeCountryDf3= allData[searchForCloseCountry3]      
        
        #FILTERS RESULTS
        resultDf1 = dfOfCountry[["deathsPer1mPop","country"]]
        resultDf1 = resultDf1[~resultDf1.index.duplicated(keep='first')]
        
        resultDf2 = closeCountryDf1[["deathsPer1mPop","country"]]
        resultDf2 = resultDf2[~resultDf2.index.duplicated(keep='first')]
        
        resultDf3 = closeCountryDf2[["deathsPer1mPop","country"]]
        resultDf3 = resultDf3[~resultDf3.index.duplicated(keep='first')]
        
        resultDf4 = closeCountryDf3[["deathsPer1mPop","country"]]
        resultDf4 = resultDf4[~resultDf4.index.duplicated(keep='first')]
        
        #PUT ALL DF TOGETHER
        finalDf = pd.concat([resultDf1,resultDf2,resultDf3,resultDf4],axis=1)
        
        print(finalDf)
        print(finalDf.describe())
        
        #GRAPHS IT
        try:
            self.__visualizer.visualize(finalDf)
        except:
            print("Could not create graph")
    
    #AS MODULAR AS I COULD MAKE FOR QUERIES
    def getAllDfs(self,**kwargs):
        queryList = []
        for key, value in kwargs.items():
            queryList.append(value)
        #CHECK IF ITS QUESTION 7,8 OR 9
        if(len(queryList) == 6):
            df1 = pd.read_sql_query(queryList[0],self.__dbObj)
            df2 = pd.read_sql_query(queryList[1],self.__dbObj)
            df3 = pd.read_sql_query(queryList[2],self.__dbObj)
            df4 = pd.read_sql_query(queryList[3],self.__dbObj)
            df5 = pd.read_sql_query(queryList[4],self.__dbObj)
            df6 = pd.read_sql_query(queryList[5],self.__dbObj)
            
            outDf = pd.concat([df1,df2,df3,df4,df5,df6]).set_index("currentDate")
        else:
            df1 = pd.read_sql_query(queryList[0],self.__dbObj)
            df2 = pd.read_sql_query(queryList[1],self.__dbObj)
            df3 = pd.read_sql_query(queryList[2],self.__dbObj)
            outDf = pd.concat([df1,df2,df3]).set_index("currentDate")

        return outDf
    
    #GETS THE LONGEST BORDER OF SPECIFIED COUNTRY
    def getLongestBorder(self,dfOfCountry,country):
        df = dfOfCountry[["closeCountry","borderLenght"]]
        df.reset_index()
        myMax = 0
        count = 0
        for i in df.index:
            if myMax < df["borderLenght"][count]:
                myMax = df["borderLenght"][count]
            count=count+1
        return myMax