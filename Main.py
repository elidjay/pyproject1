#Main Program - Elidjay Ross & William Trudel
from HandleJson import HandleJson
from WebScraper import WebScraper
from DbManager import DbManager
from DataExplorer import DataExplorer

class Main:
    
    def __init__(self):
        Main.main_program(self)
    
    def main_program(self):
        
        option = 10
        check = 0
        while(option != "0"):
            self.printMenu()
            if(check == 0):
                print("Dont forget to scrape data first!")
            elif(check == 1):
                print("Dont forget to init database!")
            
            
            option = input("Please input a number of your choice, 0 to exit: ")
            print(option)
            if(option == "1" and check == 0):
                
                print("Scraping data please wait...")
                ws1 = WebScraper("https://www.worldometers.info/coronavirus/","local_html/local_page2022-03-22.html")
                ws2= WebScraper("https://www.worldometers.info/coronavirus/","local_html/local_page2022-03-25.html")
    
                data1 = ws1.htmlScraper()
                data2 = ws2.htmlScraper()
                print("Scraping complete!")
                check = check +1
                
            elif(option == "2" and check == 1):
                print("Initiating Database...")
                try:
                    dbman = DbManager()
                    
                    dbman.create_table("day1", self.getDayTableSchema())
                    dbman.create_table("day2", self.getDayTableSchema())
                    dbman.create_table("day3", self.getDayTableSchema())
                    dbman.create_table("day4", self.getDayTableSchema())
                    dbman.create_table("day5", self.getDayTableSchema())
                    dbman.create_table("day6", self.getDayTableSchema())
                    
                    dbman.populate_table("day1",data1[2])
                    dbman.populate_table("day2",data1[1])
                    dbman.populate_table("day3",data1[0])
                    dbman.populate_table("day4",data2[2])
                    dbman.populate_table("day5",data2[1])
                    dbman.populate_table("day6",data2[0])
                except:
                    print("There was an error creating and populating the tables... Sending you back!")
                    option = 10
                
                try:
                    print("Loading Json...")
                    json = HandleJson("countries_json/country_neighbour_dist_file.json")
                    json.jsonToList()
                except:
                    print("There was an error loading your json file... Sending you back!")
                    option = 10
                
                try:
                    dbman.create_table("country_borders_table",self.getCountryBordersTableSchema())
                    dbman.populate_table("country_borders_table", json.jsonToList())
                    dex = DataExplorer(dbman.getDbObj())

                    print("Db init successful!")
                    check = check + 1
                except:
                    print("There was an error putting the json in the database... Sending you back!")
                    option = 10
            
            #6 DAYS EVOLUTION
            elif(option == "3" and check == 2):
                country = input("Please enter your country of choice: ")
                try:
                    dex.evolutionOfCountry(country)
                except:
                    print("Could not analyze " + country)
                    dbman.close_database()
                    check = 1
                    print("Please re init database ")

            #6 DAYS NEW CASES
            elif(option == "4" and check == 2):
                country = input("Please enter your country of choice: ")
                try:
                    dex.evolutionOfCountryWithBorder(country)
                except:
                    print("Could not analyze " + country)
                    dbman.close_database()
                    check = 1
                    print("Please re init database ")

            #3 DAYS WITH BORDER COUNTRIES
            elif(option == "5" and check == 2):
                country = input("Please enter your country of choice: ")
                try:
                    dex.evolutionOfDeaths(country)
                except:
                    print("Could not analyze " + country)
                    dbman.close_database()
                    check = 1
                    print("Please re init database ")

            else:
                print("Wrong input! Sending you back...")
                option = 10

        dbman.close_database()
        
        
        
    #RETURN SCHEMA        
    def getDayTableSchema(self):
        return """(
        `currentDate` varchar(10) NOT NULL,
        `rankNum` int(11) NOT NULL,
        `country` varchar(50) NOT NULL,
        `totalCases` int(11) DEFAULT NULL,
        `newCases` int(11) DEFAULT NULL,
        `totalDeaths` int(11) DEFAULT NULL,
        `newDeaths` int(11) DEFAULT NULL,
        `totalRecovered` int(11) DEFAULT NULL,
        `newRecovered` int(11) DEFAULT NULL,
        `activeCases` int(11) DEFAULT NULL,
        `seriousCases` int(11) DEFAULT NULL,
        `totalCasesPer1mPop` int(11) DEFAULT NULL,
        `deathsPer1mPop` int(11) DEFAULT NULL,
        `totalTests` int(11) DEFAULT NULL,
        `testsPer1mPop` int(11) DEFAULT NULL,
        `population` int(11) DEFAULT NULL,
        `continent` varchar(20) DEFAULT NULL,
        PRIMARY KEY (`rankNum`)     
        )"""
    
    #RETURN SCHEMA
    def getCountryBordersTableSchema(self):
        return """(
        `country` varchar(50) NOT NULL,
        `closeCountry` varchar(50) NOT NULL,
        `borderLenght` decimal(10,2) NOT NULL)"""
    
    #PRINTS MENU
    def printMenu(self):
        print("-_-_-_-Data Scraper-_-_-_-")
        print("1. Scrape Saved Websites")
        print("2. Initiate and Populate Database")
        print("3. Use Evolution of 6 Days")
        print("4. Use New Cases in 6 Days")
        print("5. Use Deaths Per One Million")
        print("-_-_-_-Data Scraper-_-_-_-")
        
        
        


m = Main()