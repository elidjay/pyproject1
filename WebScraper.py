#Elidjay Ross / William Trudel - Web Scraper Module
from urllib.request import urlopen
import requests
from bs4 import BeautifulSoup

class WebScraper:
    
    def __init__(self,url,path):
        self.__url = url
        self.__path = path
        
        #GLOBAL COUNT FOR DATES
        self.__globalCount = self.setCount()
        
        
    #SAVE WEBSITE TO LOCAL FOLDER
    def saveWebsite(self):
        req = requests.get(self.__url)
        open(self.__path, 'wb').write(req.content)

    #GETS ALL THE DATA FROM LOCAL WEBSITES
    def getData(self):
        html = urlopen("file:" + self.__path)
        return html
    
    def htmlScraper(self):
        
        html = self.getData()
        soup = BeautifulSoup(html,features="lxml")
        data = soup.find_all("table")
        soup = BeautifulSoup(str(data),features="lxml")
        data = soup.find_all("tbody")
        count = 0
        tempList = []
        
        #DELETE 2ND AND 3RD TBODY OF EACH TABLE AS ITS UNIMPORTANT
        while(count != 9):
            tempList.append(data[count])
            count = count + 3
        
        tempList = str(tempList).split("</tbody>")
        tempList.pop(3)
        dataList = []
        
        #CALL METHODS TO DEAL WITH DATA
        for i in tempList:
            i = self.getRows(i)
            i = self.cleanRows(i)
            i = self.addDates(i)
            i = self.castTypes(i)
            dataList.append(i)
        
        return dataList
    
    #GET ALL ROWS
    def getRows(self,data):
        data = str(data).translate({ord(i): "" for i in ","})
        data = str(data).translate({ord(i): "" for i in "+"})
        
        #MAKE IT SO .SPLIT("<TR>")
        data = data.replace("</tr>","<td>$</td></tr>")
        soup = BeautifulSoup(str(data),features="lxml")
        data = soup.find_all("td")
        soup = BeautifulSoup(str(data),features="lxml")
        data = str(soup.text).split("$")
        return data
    
    def cleanRows(self,dataList):
        #REMOVE ALL "TOTAL_ROW_WORLD" ROWS
        count = 0
        while(count != 8):
            dataList.pop(0)
            count = count + 1
            
        #POP LAST RANDOM THING
        dataList.pop(227)
        
        tempList = []
        for i in dataList:
            tempList.append(i.split(","))
        
        cleanedDataList = []
        for i in tempList:
            i.pop(0)
            #REMOVES EVERYTHING AFTER CONTINENT
            count = 16
            while(count != 23):
                i.pop(16)
                count = count + 1
            cleanedDataList.append(i)
        
        #STRIPS THEN SORTS DATA
        cleanedDataList = self.stripData(cleanedDataList)
        cleanedDataList = self.listSorter(cleanedDataList)
        
        return cleanedDataList
        
    #GETS RID OF ALL WHITESPACE
    def stripData(self,inputList):
        outputList = []
        for i in inputList:
            tempList = []
            for j in i:
                tempList.append(j.strip())       
            outputList.append(tempList)
        return outputList
    
    #SORTS LIST
    def listSorter(self,inputList):
        inputList.sort(key = lambda x: int(x[2]), reverse=True)
        count = 1
        while(count <= len(inputList)):
            inputList[count-1][0] = count
            count = count + 1
        return inputList
    
    #ADDS DATE FOR SQL
    def addDates(self,inputList):
        outputList = []
        
        #USES GLOBAL COUNTER TO FIGURE OUT THE DATE
        for i in inputList:
            i.insert(0,"2022-03-" + str(self.__globalCount))
            outputList.append(i)

        self.__globalCount = self.__globalCount - 1
        return outputList
    
    #SETS GLOBAL COUNT DEPENDING ON FILE
    def setCount(self):
        if("2022-03-22" in self.__path):
            count = 22
        elif("2022-03-25" in self.__path):
            count = 25
        return count
    
    #CASTS ALL DATA TO PROPER TYPE
    def castTypes(self,inputList):
        outputList = []

        for i in inputList:
            count = 0
            tempList = []
            for j in i:
                #SQL KNOWS NONE = NULL
                if(j == '' or j == 'N/A'):
                    j = None
                
                #CONVERT ALL NONE STRING TO INT
                if(count != 0 and count != 2 and count != 16 and j != None):
                    j = int(j)
                tempList.append(j)
                count = count + 1
                
            tempList = tuple((tempList))
            outputList.append(tempList)
        return outputList
                    
                    
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
        